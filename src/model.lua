require 'nn'
require 'sys'
require 'utils'
require 'string'
--require 'Dropout'

torch.setnumthreads(1)

-- parse command line arguments
if not params then
    cmd = torch.CmdLine()
    cmd:text()
    cmd:option('-src', 'data/train/train.en', 'source bitext')
    cmd:option('-trg', 'data/train/train.ru.tagged', 'target bitext')
    cmd:option('-a', 'data/train/train.align', 'alignment file')
    cmd:option('-morph', 'morph.info', 'morphology file')
    cmd:option('-niter', 10, 'number of iterations')
    cmd:option('-m', '', 'load previously trained model!')
    cmd:option('-save', '', 'dir to save models')
    cmd:option('-topn', 1, 'accuracy at top N')
    cmd:option('-train', false, 'training mode')
    cmd:option('-mode', 'word', 'train word|lemma|tag')
    cmd:option('-nonlinear', 'rectifier', 'nonlinearity: tanh, sigmoid, rectifier')
    cmd:option('-emb', 100, 'embedding size')
    cmd:option('-lr', 1e-3, 'learning rate')
    cmd:option('-window', 3, 'window size')
    cmd:option('-hidden', 200, 'number of hidden units')
    cmd:text()
    params = cmd:parse(arg)
end


-- set some more additional variables
UNKNOWN = '<UNK>'
DELIM = '|'
local DEBUG = true

-- load lexical info
local morphInfo = torch.load(params.morph)
logger(string.format('Loading %s\n', params.morph))
srcDict = morphInfo.srcDict
srcVocabSize = morphInfo.srcVocabSize

EXTRA_WORDS = {UNKNOWN, '<s>', '</s>'}
for _,w in pairs(EXTRA_WORDS) do
    srcVocabSize = srcVocabSize + 1
    srcDict[w] = srcVocabSize
end

word2lemma = morphInfo.word2lemma
lemma2id = morphInfo.lemma2id
nlemmas = morphInfo.nlemmas
ntags = morphInfo.ntags
lemmaCandidates = morphInfo.lemmaCandidates
lemmaWordPos = morphInfo.lemmaWordPos
tag2id = morphInfo.tag2id
tagCandidates = morphInfo.tagCandidates
inflect = morphInfo.inflect
tagLemmaPos = morphInfo.tagLemmaPos

-- parameters for NN
HUs = params.hidden
windowSize = params.window
N = windowSize*2+1
learningRate = params.lr
learningRateDecay = 1e-4
embeddingSize = params.emb
trainMode = params.mode

nSents = 0
for line in io.lines(params.a) do nSents = nSents + 1 end

logger(string.format('\nSummarization:\n'))

logger(string.format('==> vocabulary size: %d\n', srcVocabSize))
logger(string.format('==> number of lemmas: %d\n', nlemmas))
logger(string.format('==> number of morphological tags: %d\n', ntags))
logger(string.format('==> number of training sentences: %d\n', nSents))
logger(string.format('==> number of hidden units: %d\n', HUs))
logger(string.format('==> window context size: %d\n', windowSize))
logger(string.format('==> word embeddings size: %d\n', embeddingSize))
logger(string.format('==> mode: %s\n', params.mode))

if params.train then
    nInputWords = 0
    inputSize = 0
    outputSize = 0
    vocabSize = 0

    if params.mode == 'lemma' then
        vocabSize = srcVocabSize
        nInputWords = N
        inputSize = nInputWords * embeddingSize
        outputSize = nlemmas
    end

    if params.mode == 'tag' then
        vocabSize = srcVocabSize + nlemmas
        nInputWords = N + 1
        inputSize = nInputWords * embeddingSize
        outputSize = ntags
    end

    if params.mode == 'word' then -- not implemented now
        vocabSize = srcVocabSize
        nInputWords = N
        inputSize = nInputWords * embeddingSize
        outputSize = 1e5 -- need to fix later
    end

    logger(string.format('==> creating neural network model\n'))
    --
    lookup = nn.LookupTable(vocabSize, embeddingSize)
    lookup:reset(0.01)
    concat = nn.Reshape(inputSize)
    hidWeight = torch.Tensor(HUs, inputSize)
    hidBias = torch.Tensor(HUs)
    local stdv = 1./math.sqrt(hidWeight:size(2))
    hidWeight:uniform(-stdv, stdv)
    hidBias:uniform(-stdv, stdv)
    --
    hidNeurons = torch.Tensor(HUs)
    gradHid = torch.Tensor(HUs) -- gradient to hidden layer
    gradIn = torch.Tensor(inputSize) -- gradient to input layer
    -- output layer
    outWeight = torch.Tensor(outputSize, HUs)
    outBias = torch.Tensor(outputSize)
    local stdvO = 1./math.sqrt(outWeight:size(2))
    outWeight:uniform(-stdvO, stdvO)
    outBias:uniform(-stdvO, stdvO)
    if params.nonlinear == 'tanh' then
        nonLinear = nn.Tanh()
    elseif params.nonlinear == 'sigmoid' then
        nonLinear = nn.Sigmoid()
    elseif params.nonlinear == 'rectifier' then
        nonLinear = nn.Threshold(0,0)
    else
        error('Unknown non-linearity function\n')
    end
    softmax = nn.SoftMax()
else
    logger(string.format('==> load model: %s\n', params.m))
    local net = torch.load(params.m)
    model = net.model
    ocf = net.ocf
end