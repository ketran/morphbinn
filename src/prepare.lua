require 'utils'
require 'nn'
require 'string'
require 'sys'
-- get all necessary morphological info to train a joint stem-suffix model
if not params then
    cmd = torch.CmdLine()
    cmd:option('-tf', 'train200K.ru.morph', 'target annotated with tree-tagger')
    cmd:option('-sd', 'train.en.dictF5', 'source dictionary')
    cmd:option('-save', 'morph.info', 'serialize the data')
    cmd:option('-prune', 'lexicon.en-ru.f5p2o10.top200', 'use extra infomation')
    params = cmd:parse(arg)
end

-- data format
-- target file: each line has format of word|tag|lemma
-- where tag and lemma are obtained form tree-tagger

local DEBUG = true -- for debugging purpose
local DELIM = '|'
-- define all the variables
inflect = {}
tagCandidates = {} -- possible tags of lemmas
tag2id = {} -- mapping tags into numbers
lemma2id = {} -- mapping lemmas to numbers

-- variables for stem model
word2lemma = {} -- hash table that maps word into its lemma
-- counting
nlemmas = 0
ntags = 0

logger(string.format('Reading target file %s\n', params.tf))
for sent in io.lines(params.tf) do
    local trg_toks = split(sent, ' ')
    for _,tok in pairs(trg_toks) do
        local cols = split(tok, DELIM)
        local word, tag, lemma = cols[1], cols[2], cols[3]
        word2lemma[word] = lemma
        -- update lemma
        if not lemma2id[lemma] then
            nlemmas = nlemmas + 1
            lemma2id[lemma] = nlemmas
        end
        -- update tag
        if not tag2id[tag] then
            ntags = ntags + 1
            tag2id[tag] = ntags
        end
        -- update inflection table
        inflect[lemma] = inflect[lemma] or {}
        inflect[lemma][tag] = word -- no encode into numbers
    end
end

-- auxiliary variables for suffix model
tagLemmaPos = {}

for lemma, tags in pairs(inflect) do
    local t = {}
    local p = {} -- relative position

    for tag,_ in pairs(tags) do
        t[#t+1] = tag2id[tag]
        p[tag] = #t
    end
    tagCandidates[lemma] = t
    tagLemmaPos[lemma] = p
end

-- read source dictionary file
logger(string.format('Reading source dictionary file %s\n', params.sd))
srcDict = {}
srcVocabSize = 0
for w in io.lines(params.sd) do
    srcVocabSize = srcVocabSize + 1
    srcDict[w] = srcVocabSize
end

-- read lexicon file
lemmaCandidates = {} -- map a source word to its possible lemmas
lemmaWordPos = {} -- given a source word and a target lemma
-- return a lemma position in the list of possible lemmas
logger(string.format('Pruning translation candidates according to %s\n', params.prune))
for line in io.lines(params.prune) do
    local cols = split(line, '\t')
    local src_word = cols[1]
    local transCandidates = split(cols[2], ' ')
    local lemma_cands = {} -- lemma candidates of a source word
    for _, w in pairs(transCandidates) do
        if word2lemma[w] then
            local lemma = word2lemma[w]
            lemma_cands[lemma] = true
        end
    end

    local lemmaIds = {} -- get lemma ids of target lemmas
    local p = {}
    for lemma,_ in pairs(lemma_cands) do
        lemmaIds[#lemmaIds+1] = lemma2id[lemma]
        p[lemma] = #lemmaIds
    end
    lemmaCandidates[src_word] = lemmaIds
    lemmaWordPos[src_word] = p
end

logger(string.format('saving all the information to %s\n', params.save))
morphInfo = {
    srcDict = srcDict,
    srcVocabSize = srcVocabSize,
    word2lemma = word2lemma,
    lemma2id = lemma2id,
    nlemmas = nlemmas,
    ntags = ntags,
    lemmaCandidates = lemmaCandidates,
    lemmaWordPos = lemmaWordPos,
    tag2id = tag2id,
    tagCandidates = tagCandidates,
    inflect = inflect,
    tagLemmaPos = tagLemmaPos
}

os.execute('mkdir -p ' .. sys.dirname(params.save))
logger(string.format('==> Saving information to %s\n', params.save))
torch.save(params.save, morphInfo)
