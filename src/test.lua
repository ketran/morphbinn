require 'model'
require 'xlua'


function test(srcFile, trgFile, alignedFile, topN)
    logger(string.format('source file: %s\ntarget file: %s\n, alignment file: %s\n', srcFile, trgFile, alignedFile))
    criterion = nn.ClassNLLCriterion()
    local srcReader = io.lines(srcFile)
    local trgReader = io.lines(trgFile)
    local alignedReader = io.lines(alignedFile)
    local sentCounter = 0
    local wCounter = 0
    local err = 0
    local nCorrect = 0
    local nOb = 0 -- number of observed data
    while true do
        sentCounter = sentCounter + 1
        local srcSent = srcReader()
        if not srcSent then break end

        local trgSent = trgReader()
        local alignedLink = alignedReader()
        srcSent = string.rep("<s> ", windowSize) .. srcSent .. string.rep(" </s>", windowSize)
        local srcTokens = split(srcSent, ' ')
        local trgTokens = split(trgSent, ' ')
        local align = split(alignedLink, ' ')

        for i=1, #align do
            -- rewrite from here
            wCounter = wCounter + 1
            local link = split(align[i], '-')
            local srcTok = srcTokens[ link[1] + 1 + windowSize]
            local trgTok = trgTokens[ link[2] + 1 ]
            local cols = split(trgTok, DELIM)
            local word, tag, lemma = cols[1], cols[2], cols[3]
            -- check mode
            local to_learn = (trainMode == 'lemma' and lemmaCandidates[srcTok] and lemmaWordPos[srcTok][lemma]) or
                (trainMode == 'tag' and tagCandidates[lemma] and tagLemmaPos[lemma][tag])
            if not to_learn then
                goto continue
            end
            
            local context = {}
            local s, e = link[1] + 1, link[1] + N

            for j=s,e do
                context[#context + 1] = srcDict[srcTokens[j]] or srcDict[UNKNOWN]
            end

            if trainMode == 'lemma' then
                input = torch.LongTensor(context)
                cands = torch.LongTensor(lemmaCandidates[srcTok])
                target = lemmaWordPos[srcTok][lemma]
            end
            if trainMode == 'tag' then
                context[#context + 1] = lemma2id[lemma] + srcVocabSize
                input = torch.LongTensor(context)
                cands = torch.LongTensor(tagCandidates[lemma])
                target = tagLemmaPos[lemma][tag]
            end

            -- start training here
            -- wCounter = wCounter + 1
            nOb = nOb + 1
            local outLayer = nn.Sequential()
            outLayer:add(nn.Linear(HUs, cands:nElement()))
            outLayer:add(nn.LogSoftMax())
            outLayer:get(1).weight:set(ocf.weight:index(1, cands))
            outLayer:get(1).bias:set(ocf.bias:index(1, cands))

            local hid = model:forward(input)
            local pred = outLayer:forward(hid)
            
            local t = math.min(topN, cands:nElement())
            local v,id = pred:sort(1, true)
            for j=1,t do
                if id[j] == target then
                    nCorrect = nCorrect + 1
                end
            end
            err = err + criterion:forward(pred, target)
            ::continue::
        end
        if sentCounter % 1e3 ==0 then
            xlua.progress(sentCounter, nSents)
            logger(string.format('\n==> acc at %d: %.5f\n', topN, nCorrect*100/wCounter))
            logger(string.format('==> percentage of observed data: %.5f\n', nOb/wCounter))
        end
    end
    logger(string.format(string.rep('-', 42) .. '\n'))
    logger(string.format('\n==> acc at %d: %.5f\n', topN, nCorrect*100/wCounter))
end

test(params.src, params.trg, params.a, params.topn)
