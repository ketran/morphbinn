require 'model'
require 'xlua'

function saveNet(filename)
    -- pack everything
    -- remove gradWeight and gradBias
    local model = nn.Sequential()
    model:add(lookup:clone())
    model:get(1).gradWeight = nil
    model:add(concat)
    model:add(nn.Linear(inputSize, HUs))
    model:get(3).weight:copy(hidWeight)
    model:get(3).bias:copy(hidBias)
    model:get(3).gradWeight = nil
    model:get(3).gradBias = nil
    model:add(nonLinear)

    ocf = nn.Linear(HUs, outputSize)
    ocf.weight:copy(outWeight)
    ocf.bias:copy(outBias)
    ocf.gradWeight = nil
    ocf.gradBias = nil
    torch.save(filename, {model = model, ocf = ocf})
end

function train(srcFile, trgFile, alignedFile, iter)
    logger(string.format('source file: %s\ntarget file: %s\n, alignment file: %s\n', srcFile, trgFile, alignedFile))
    logger(string.format('%s | iter: %d\n', os.date(), iter))
    local time = sys.clock()
    learningRate = learningRate/(1+iter*learningRateDecay)

    local srcReader = io.lines(srcFile)
    local trgReader = io.lines(trgFile)
    local alignedReader = io.lines(alignedFile)
    local sentCounter = 0
    local wCounter = 0
    local err = 0
    while true do
        sentCounter = sentCounter + 1
        local srcSent = srcReader()
        if not srcSent then break end

        local trgSent = trgReader()
        local alignedLink = alignedReader()
        srcSent = string.rep("<s> ", windowSize) .. srcSent .. string.rep(" </s>", windowSize)
        local srcTokens = split(srcSent, ' ')
        local trgTokens = split(trgSent, ' ')
        local align = split(alignedLink, ' ')

        for i=1, #align do
            -- rewrite from here
            local link = split(align[i], '-')
            local srcTok = srcTokens[ link[1] + 1 + windowSize]
            local trgTok = trgTokens[ link[2] + 1 ]
            local cols = split(trgTok, DELIM)
            local word, tag, lemma = cols[1], cols[2], cols[3]
            -- check mode
            local to_learn = (trainMode == 'lemma' and lemmaCandidates[srcTok] and lemmaWordPos[srcTok][lemma]) or
                (trainMode == 'tag' and tagCandidates[lemma] and tagLemmaPos[lemma][tag])
            if not to_learn then
                goto continue
            end

            local context = {}
            local s, e = link[1] + 1, link[1] + N

            for j=s,e do
                context[#context + 1] = srcDict[srcTokens[j]] or srcDict[UNKNOWN]
            end

            if trainMode == 'lemma' then
                input = torch.LongTensor(context)
                cands = torch.LongTensor(lemmaCandidates[srcTok])
                target = lemmaWordPos[srcTok][lemma]
            end
            if trainMode == 'tag' then
                context[#context + 1] = lemma2id[lemma] + srcVocabSize
                input = torch.LongTensor(context)
                cands = torch.LongTensor(tagCandidates[lemma])
                target = tagLemmaPos[lemma][tag]
            end

            wCounter = wCounter + 1
            -- forward pass
            lookup:forward(input)
            local inVec  = concat:forward(lookup.output)
            hidNeurons:copy(hidBias)
            hidNeurons:addmv(hidWeight, inVec)
            local hid = nonLinear:forward(hidNeurons)

            local o = torch.Tensor(cands:nElement())
            o:copy(outBias:index(1, cands))
            o:addmv(outWeight:index(1, cands), hid)
            local probs = softmax:forward(o)

            -- backward
            if probs[target] == 0 then
                probs[target] = 1e-6 --add a small number
            end
            err = err - math.log(probs[target])

            local grad = probs
            grad[target] = grad[target] - 1 -- turn to grad
            gradHid:mv(outWeight:index(1, cands):t(), grad)
            outWeight:indexCopy(1, cands, outWeight:index(1,cands):addr(-learningRate, grad, hid))
            outBias:indexCopy(1, cands, outBias:index(1, cands):add(-learningRate, grad))
            local gradNonLinear = nonLinear:backward(hidNeurons, gradHid)
            hidWeight:addr(-learningRate, gradNonLinear, inVec)
            hidBias:add(-learningRate, gradNonLinear)
            gradIn:mv(hidWeight:t(), gradNonLinear)
            lookup:accUpdateGradParameters(input, gradIn:reshape(nInputWords, embeddingSize), learningRate)

            ::continue::
        end
        if sentCounter % 1e3 ==0 then
            xlua.progress(sentCounter, nSents)
            logger(string.format('\n==> time to learn 1 example: %.5f ms\n', (sys.clock()-time)*1000/wCounter))
            logger(string.format('==> training log-likelihood: %.5f\n', err/wCounter))
            collectgarbage()
        end
    end
    local filename = paths.concat(params.m .. '.it' .. iter)
    os.execute('mkdir -p ' .. sys.dirname(filename))
    print(string.format('\n==> saving model to %s\n', filename))
    saveNet(filename)
end

-- training here
for i=1, params.niter do
    train(params.src, params.trg, params.a, i)
end
